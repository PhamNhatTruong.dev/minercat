import { _decorator, Component, Node, tween, Vec3 } from 'cc';
import { IState } from '../../Gamerical/StateMachine/IState';
import StateMachine from '../../Gamerical/StateMachine/StateMachine';
import { RCatController } from '../RCatController';
import { AsNode } from '../../Gamerical/Utility/AsNode';
import { MapBlockType } from '../../Gamerical/TerrainManager/MapBlockType';
import { GameDirector } from '../../Gamerical/Utility/GameDirector';
const { ccclass, property } = _decorator;

@ccclass('RCatReturnBaseState')
export class RCatReturnBaseState implements IState {
    _name: string;
    _stateMachine: StateMachine;
    _controller : RCatController;

    constructor(stateMachine : StateMachine){
        this._stateMachine = stateMachine;
        this._controller = this._stateMachine as RCatController;
    }

    onEnter(): void {
        let currentPos = this._controller.currentPosition;
        let basePos = this._controller.basePosition;
        let moves = this.aStar(GameDirector.TerrainManager().resourceMap,currentPos,basePos)
        let dex = this._controller.data.dexterity;
        let runTime =  ( Math.max(1,200 - dex) ) / 200;
        let moveTween = tween(this._controller.node.position);
        for(let posIndex = 0; posIndex < moves.length ; posIndex ++){
            let pos = moves[posIndex];
            let worldPos = this._controller.getWorldPos(pos[0],pos[1]);
            moveTween.to(runTime,new Vec3(worldPos[0],worldPos[1],0), {
                onUpdate : (target : Vec3, ratio : number) => {
                    this._controller.node.position = target;
                }
            });
        }
        this._controller.currentPosition = basePos;
        moveTween.call(this.moveCompleted.bind(this));
        moveTween.start();
    }

    onUpdate(dt: number): void {
    }

    onExit(): void {
    }
 
    private moveCompleted(){
        GameDirector.SoundManager().playSfx("res_drop");
        this._stateMachine.setState("idle");
    }

    private aStar(grid: number[][], start: [number, number], target: [number, number]): [number, number][] | null {
        const dx = [1, 0, -1, 0];
        const dy = [0, 1, 0, -1];
    
        const isValid = (x: number, y: number): boolean => {
            return x > 0 && x < grid.length && y > 0 && y < grid[0].length && grid[y][x] != MapBlockType.WALL;
        };
    
        const calculateHeuristic = (x: number, y: number): number => {
            return Math.abs(x - target[0]) + Math.abs(y - target[1]); // Manhattan distance
        };
    
        const openList: AsNode[] = [];
        const closedList: AsNode[] = [];
    
        const startNode: AsNode = { x: start[0], y: start[1], g: 0, h: 0, f: 0 };
        openList.push(startNode);
    
        while (openList.length > 0) {
            openList.sort((a, b) => a.f - b.f);
            const currentNode = openList.shift()!;
            closedList.push(currentNode);
    
            if (currentNode.x === target[0] && currentNode.y === target[1]) {
                const path: [number, number][] = [];
                let current: AsNode | undefined = currentNode;
                while (current) {
                    path.push([current.x, current.y]);
                    current = current.parent;
                }
                return path.reverse();
            }
    
            for (let i = 0; i < 4; i++) {
                const newX = currentNode.x + dx[i];
                const newY = currentNode.y + dy[i];
    
                if (!isValid(newX, newY)) {
                    continue;
                }
    
                const gCost = currentNode.g + 1;
                const hCost = calculateHeuristic(newX, newY);
                const fCost = gCost + hCost;
    
                const existingOpenIndex = openList.findIndex(node => node.x === newX && node.y === newY);
                const existingClosedIndex = closedList.findIndex(node => node.x === newX && node.y === newY);
    
                if (existingClosedIndex !== -1 && closedList[existingClosedIndex].f <= fCost) {
                    continue;
                }
    
                if (existingOpenIndex === -1 || fCost < openList[existingOpenIndex].f) {
                    const newNode: AsNode = { x: newX, y: newY, g: gCost, h: hCost, f: fCost, parent: currentNode };
                    if (existingOpenIndex !== -1) {
                        openList[existingOpenIndex] = newNode;
                    } else {
                        openList.push(newNode);
                    }
                }
            }
        }
    
        return null;
    }
}


