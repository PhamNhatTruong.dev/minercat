import { Game, tween, Vec3 } from "cc";
import { IState } from "../../Gamerical/StateMachine/IState";
import StateMachine from "../../Gamerical/StateMachine/StateMachine";
import { MapBlockType } from "../../Gamerical/TerrainManager/MapBlockType";
import { AsNode } from "../../Gamerical/Utility/AsNode";
import { GameDirector } from "../../Gamerical/Utility/GameDirector";
import { RCatController } from "../RCatController";
import { Queue } from "../../Gamerical/Utility/Queue";

export class RCatRunToNearestResourceState implements IState {
    _name: string;
    _stateMachine: StateMachine;
    _controller : RCatController;
    _nearestResPos : [number,number];

    constructor(stateMachine : StateMachine){
        this._stateMachine = stateMachine;
        this._controller = this._stateMachine as RCatController;
    }

    onEnter(): void {
        this._nearestResPos = this.findNearResourcePosition(5);
        if(!this._nearestResPos){
            //that mean not found avaiable 
            this._stateMachine.setState("idle");
            return;
        }
        let moves = this.aStar(GameDirector.TerrainManager().resourceMap,this._controller.currentPosition,this._nearestResPos);
        if(!moves){
            this._stateMachine.setState("idle");
        }
        let dex = this._controller.data.dexterity;
        let runTime =  ( Math.max(1,200 - dex) ) / 200;
        let moveTween = tween(this._controller.node.position);
        for(let posIndex = 0; posIndex < moves.length ; posIndex ++){
            let pos = moves[posIndex];
            let worldPos = this._controller.getWorldPos(pos[0],pos[1]);

            moveTween.to(runTime,new Vec3(worldPos[0],worldPos[1],0), {
                onUpdate : (target : Vec3, ratio : number) => {
                    if(this._controller.node.position.x < target.x){
                        console.log(`L`);
                        this._controller.node.eulerAngles = new Vec3(0,180,0);
                    }else{
                        console.log(`R`);
                        this._controller.node.eulerAngles = new Vec3(0,0,0);
                    }
                    this._controller.node.position = target;
                }
            });
        }
        this._controller.currentExploitPosition = this._nearestResPos;
        moveTween.call(this.moveCompleted.bind(this));
        moveTween.start();
    }

    onUpdate(dt: number): void {

    }
    onExit(): void {
    }

    private moveCompleted(){
        this._controller.currentPosition = this._nearestResPos;
        this._stateMachine.setState("exploit");
    }

    private findNearResourcePosition(availablePosLimit: number): [number, number] | null {
        function shuffleArray<T>(array: T[]): T[] {
            for (let i = array.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [array[i], array[j]] = [array[j], array[i]];
            }
            return array;
        }
    
        const resourceMap = GameDirector.TerrainManager().resourceMap;
        const toughnessMap = GameDirector.TerrainManager().toughnessMap;
        const currentPosition = this._controller.currentPosition;
        const availablePos: [number, number][] = [];
        const checkedMap: boolean[][] = Array.from({ length: resourceMap.length }, () => Array(resourceMap[0].length).fill(false));
        const posQueue = new Queue<[number, number]>();
      
        posQueue.enqueue([currentPosition[0],currentPosition[1]]);

        while (!posQueue.isEmpty()) {
            const currentCheckPos = posQueue.dequeue();
            const [currentX, currentY] = currentCheckPos;
    
            if (checkedMap[currentY][currentX]) {
                continue;
            }
            checkedMap[currentY][currentX] = true;
    
            const dx = [1, 0, -1, 0];
            const dy = [0, 1, 0, -1];
            const shuffledIndices = shuffleArray([0, 1, 2, 3]);
    
            for (const index of shuffledIndices) {
                const posX = currentX + dx[index];
                const posY = currentY + dy[index];
    
                if (posX < 0 || posX >= resourceMap[0].length || posY < 0 || posY >= resourceMap.length) {
                    continue;
                }
    
                if (resourceMap[posY][posX] === MapBlockType.WALL) {
                    continue;
                }
    
                if ((toughnessMap[posY][posX] & (1 << 7)) !== 0) {
                    continue;
                }
    
                if (resourceMap[posY][posX] === MapBlockType.BRICK) {
                    availablePos.push([posX, posY]);
                    if (availablePos.length >= availablePosLimit) {
                        const chosenPos = shuffleArray(availablePos)[0];
                        toughnessMap[chosenPos[1]][chosenPos[0]] |= (1 << 7);    
                        GameDirector.TerrainManager().toughnessMap = toughnessMap;
                        return chosenPos;
                    }
                }
    
                if (resourceMap[posY][posX] === MapBlockType.FLOOR || resourceMap[posY][posX] === MapBlockType.BASE) {
                    posQueue.enqueue([posX, posY]);
                }
            }
        }
    
        // Return null if no suitable position is found
        return null;
    }
    private aStar(grid: number[][], start: [number, number], target: [number, number]): [number, number][] | null {
        const dx = [1, 0, -1, 0];
        const dy = [0, 1, 0, -1];
    
        const isValid = (x: number, y: number): boolean => {
            return x > 0 && x < grid.length && y > 0 && y < grid[0].length && grid[y][x] != MapBlockType.WALL;
        };
    
        const calculateHeuristic = (x: number, y: number): number => {
            return Math.abs(x - target[0]) + Math.abs(y - target[1]); // Manhattan distance
        };
    
        const openList: AsNode[] = [];
        const closedList: AsNode[] = [];
    
        const startNode: AsNode = { x: start[0], y: start[1], g: 0, h: 0, f: 0 };
        openList.push(startNode);
    
        while (openList.length > 0) {
            openList.sort((a, b) => a.f - b.f);
            const currentNode = openList.shift()!;
            closedList.push(currentNode);
    
            if (currentNode.x === target[0] && currentNode.y === target[1]) {
                const path: [number, number][] = [];
                let current: AsNode | undefined = currentNode;
                while (current) {
                    path.push([current.x, current.y]);
                    current = current.parent;
                }
                return path.reverse();
            }
    
            for (let i = 0; i < 4; i++) {
                const newX = currentNode.x + dx[i];
                const newY = currentNode.y + dy[i];
    
                if (!isValid(newX, newY)) {
                    continue;
                }
    
                const gCost = currentNode.g + 1;
                const hCost = calculateHeuristic(newX, newY);
                const fCost = gCost + hCost;
    
                const existingOpenIndex = openList.findIndex(node => node.x === newX && node.y === newY);
                const existingClosedIndex = closedList.findIndex(node => node.x === newX && node.y === newY);
    
                if (existingClosedIndex !== -1 && closedList[existingClosedIndex].f <= fCost) {
                    continue;
                }
    
                if (existingOpenIndex === -1 || fCost < openList[existingOpenIndex].f) {
                    const newNode: AsNode = { x: newX, y: newY, g: gCost, h: hCost, f: fCost, parent: currentNode };
                    if (existingOpenIndex !== -1) {
                        openList[existingOpenIndex] = newNode;
                    } else {
                        openList.push(newNode);
                    }
                }
            }
        }
    
        return null;
    }
}