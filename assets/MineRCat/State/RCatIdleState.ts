import { Vec3 } from "cc";
import { IState } from "../../Gamerical/StateMachine/IState";
import StateMachine from "../../Gamerical/StateMachine/StateMachine";
import { MapBlockType } from "../../Gamerical/TerrainManager/MapBlockType";
import { GameDirector } from "../../Gamerical/Utility/GameDirector";
import { RCatController } from "../RCatController";

export class RCatIdleState implements IState{
    _name: string;
    _stateMachine: StateMachine;
    _currentTime : number;
    _nextActiveTime : number;

    
    constructor(stateMachine : StateMachine){
        this._stateMachine = stateMachine;
    }

    onEnter(): void {
        let data = (this._stateMachine as RCatController).data;
        this._currentTime = 0;
        this._nextActiveTime = 0;
    }
    onUpdate(dt: number): void {
        this._currentTime += dt;
        if(this._currentTime > this._nextActiveTime){
            this.findspawnPosition();
            this._stateMachine.setState("run2res");
        }
    }
    onExit(): void {
    }

    private findspawnPosition(){
        let map = GameDirector.TerrainManager().resourceMap;
        for(let j = 0 ; j < map.length ; j ++){
            for(let i = 0 ; i < map[0].length ; i++){
                if(map[j][i] == MapBlockType.BASE){
                    j = j + GameDirector.TerrainManager().baseSize / 2;
                    i = i + GameDirector.TerrainManager().baseSize / 2;
                    let pos = (this._stateMachine as RCatController).getWorldPos(i,j);
                    (this._stateMachine as RCatController).node.setPosition  (new Vec3(pos[0],pos[1],0));
                    (this._stateMachine as RCatController).basePosition = [i,j];
                    (this._stateMachine as RCatController).currentPosition = [i,j];
                    return;
                }
            }
        }
    }
} 

