import { GraphQlQuery } from "../../Gamerical/NetworkManager/GraphQlQuery";
import { INetworkData } from "../../Gamerical/NetworkManager/INetworkData";
import { GameDirector } from "../../Gamerical/Utility/GameDirector";
import { LoginDetail } from "./LoginDetail";

export class NetWorkCaller {

    private static BASE_URL = "http://167.172.5.228:8080";

    private static _loginDetail : LoginDetail = JSON.parse(localStorage.getItem("logindetail"));

    private static loginDetail(): LoginDetail{
        return this._loginDetail;
    }

    private static setLoginDetail(logindetail : LoginDetail) : void{
        this._loginDetail = logindetail;
        localStorage.setItem("logindetail",JSON.stringify(this._loginDetail));
    }

    public static Login(username : string , password : string){
        return new Promise( (resolve , reject) => {
            let query = `mutation{\r\nlogin(username : "${username}", password : "${password}" {${LoginDetail.getFullGraphFormat()}\r\b}`;
            let variable = "";
            let files = null;
            let graphqlQuery = new GraphQlQuery(query,variable,files);
            this.SendRequest<LoginDetail>(graphqlQuery,(result : LoginDetail) => {
                this.setLoginDetail(result);
                resolve(result);
            }, (err) => {
                console.error(`login error ${err}`);
                reject(err);
            });

        });
    }

    public static SendRequest<T extends INetworkData>(query : GraphQlQuery , onCompleted : (arg0: T) => void , onFailed : (err : string) => void ){

        var header : { [key: string]: string };
        if(this.loginDetail() != null && !this.loginDetail().token){
            header["Authorization"] = `Bearer ${this.loginDetail().token}`;
        }

        GameDirector.NetworkManager().sendGrapQlAsync<T>(NetWorkCaller.BASE_URL,header,query).then( value => {
                onCompleted(value as T);
        }).catch(err => {
            console.log(err);
        });
    }
 
}