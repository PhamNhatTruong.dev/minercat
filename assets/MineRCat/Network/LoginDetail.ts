import { INetworkData } from "../../Gamerical/NetworkManager/INetworkData";
import { User } from "./User";

export class LoginDetail implements INetworkData {
  public username!: string;
  public token!: string;
  public avatarUrl!: string;
  public user!: User;

  constructor(){
    
  }

  public static getFullGraphFormat(): string {
    return `{
        id
        username
        token
        avatarUrl
        user {
        ${User.GetFullGrapFormat()}
        }
        }`;
  }
}
