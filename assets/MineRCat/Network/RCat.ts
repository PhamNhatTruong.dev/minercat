import { INetworkData } from "../../Gamerical/NetworkManager/INetworkData";

export class RCat implements INetworkData {

    constructor(){
    
    }
    
    public id : string;
    public model_id : string;
    public model_path : string;
    public model_icon_path : string;
    public level : number;
    public exp : number;
    public next_level_exp: number;
    public strength : number;
    public dexterity : number;
    public constitution  : number;
    public intelligence : number;
    public wisdom : number;

}