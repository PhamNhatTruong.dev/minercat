import { _decorator, CCInteger, Component, Game, Node, UITransform } from 'cc';
import StateMachine from '../Gamerical/StateMachine/StateMachine';
import { RCat } from './Network/RCat';
import { RCatIdleState } from './State/RCatIdleState';
import { RCatRunToNearestResourceState } from './State/RCatRunToNearestResourceState';
import { GameDirector } from '../Gamerical/Utility/GameDirector';
import { RCatExploitState } from './State/RCatExploitState';
import { RCatReturnBaseState } from './State/RCatReturnBaseState';
 
const { ccclass, property } = _decorator;

@ccclass('RCatController')
export class RCatController extends StateMachine {

    private _data: RCat;
    private _mapWidth : number;
    private _mapHeight : number;
    private _mapContentWidth : number;
    private _mapContentHeight : number;
    private _basePosition: [number, number];
    private _currentPosition: [number, number];
    private _currentExploitPosition: [number, number];
    private _idleAnimationName: string;
    private _runAnimationName: string;
    private _exploitAnimationName: string;


    @property({
		type : CCInteger,
		visible : true,
		readonly : true
	})
	_currentExp : number;
    
    
    public get data(): RCat {
        return this._data;
    }
    public set data(value: RCat) {
        this._data = value;
    }
    public get basePosition(): [number, number] {
        return this._basePosition;
    }
    public set basePosition(value: [number, number]) {
        this._basePosition = value;
    }
    public get currentPosition(): [number, number] {
        return this._currentPosition;
    }
    public set currentPosition(value: [number, number]) {
        this._currentPosition = value;
    }
    public get currentExploitPosition(): [number, number] {
        return this._currentExploitPosition;
    }
    public set currentExploitPosition(value: [number, number]) {
        this._currentExploitPosition = value;
    }
    public get idleAnimationName(): string {
        return this._idleAnimationName;
    }
    public get runAnimationName(): string {
        return this._runAnimationName;
    }
    public get exploitAnimationName(): string {
        return this._exploitAnimationName;
    }
    start() {
        this._mapWidth = GameDirector.TerrainManager().mapWidth;
        this._mapHeight = GameDirector.TerrainManager().mapHeight;
        this._mapContentWidth = this.node.parent.getComponent(UITransform).contentSize.x;
        this._mapContentHeight = this.node.parent.getComponent(UITransform).contentSize.y;
        this.addState("idle", new RCatIdleState(this));    
        this.addState("run2res", new RCatRunToNearestResourceState(this));
        this.addState("exploit", new RCatExploitState(this));
        this.addState("returnbase",new RCatReturnBaseState(this));
    }

    onUpdate(deltaTime: number) {
        super.onUpdate(deltaTime);
    }

    public getWorldPos(x : number, y : number) : [number, number]{
        let posX = this._mapContentWidth * ( x / this._mapWidth - 0.5);
        let posY = this._mapContentHeight * (  0.5 - y / this._mapHeight);
        return [posX, posY];
    }

}


