import { _decorator, Component, Node } from 'cc';
import { CategoryController } from '../CategoryController';
import { RCat } from '../../../Network/RCat';
const { ccclass, property } = _decorator;

@ccclass('LineupTeamController')
export class LineupTeamController extends Component {
    @property({type : CategoryController})
    private category : CategoryController;

    private _rcats: RCat[];

    public get rcats(): RCat[] {
        return this._rcats;
    }
    public set rcats(value: RCat[]) {
        this._rcats = value;
    }

    protected start(): void {
        this.category.onSelectedRCat = this.onSelectedRCat.bind(this);
    }
    
    private onSelectedRCat(rCat : RCat){

    }
}


