import { GameDirector } from "../../../Gamerical/Utility/GameDirector";
import { RCat } from "../../Network/RCat";
import { RCatController } from "../../RCatController";

export class HomeLogic  {

    private _terrain : number[][];
    private _map : number[][];
    private _toughnessMap : number[][];
    private _catControllers : RCatController[];
    private _cats : RCat[];
    public generateMap( ) {
        // first must check is map generted in database
        // then load map
        // else start generate
        console.log(`Home logic generateMap`);
        return new Promise( (resolve , reject) => {
            GameDirector.TerrainManager().loadMap()
            .then(value => {
                this._terrain = value as number[][];
                GameDirector.TerrainManager().terrainMap = this._terrain;
                return GameDirector.TerrainManager().loadResource();
            }).then(resourceMap => {
                this._map = resourceMap as number[][];
                GameDirector.TerrainManager().resourceMap = this._map;
                return GameDirector.TerrainManager().loadToughness();
            }).then(toughnessMap =>{
                this._toughnessMap = toughnessMap as number[][];
                GameDirector.TerrainManager().toughnessMap = this._toughnessMap;
                resolve(this._map);
            }).catch( async err => {
                console.log(err);
                try {
                    const noiseMap = await GameDirector.TerrainManager().randomFill();
                    console.log(`Generate noise map completed`);
                    const orgiTerranMap = await GameDirector.TerrainManager().generateCellularAutomationMap(noiseMap as number[][]);
                    console.log(`Generate map completed`);
                    this._terrain = GameDirector.TerrainManager().USmoothMap(orgiTerranMap as number[][]);
                    console.log(`Smooth map completed`);
                    const campMap = await GameDirector.TerrainManager().putCampPlace(this._terrain);
                    console.log(`Put resource completed`);
                    const resourceMap = await GameDirector.TerrainManager().putResources(campMap as number[][]);
                    this._map = resourceMap as number[][];
                    console.log(`Put camp completed`);
                    const toughnessMap = await GameDirector.TerrainManager().createToughnessMap( this._map);
                    console.log(`Create toughtness map completed`);
                    this._toughnessMap = toughnessMap as number[][];
                    GameDirector.TerrainManager().terrainMap = this._terrain;
                    GameDirector.TerrainManager().resourceMap = this._map;
                    GameDirector.TerrainManager().toughnessMap = this._toughnessMap;
                    resolve(this._map);
                } catch (err) {
                    console.log(`Generate map err: ${err}`)
                    reject(err);
                }
            });
        });
        
    }


    public generateRCat() {
        return new Promise((resolve , reject) => {
            try{
                this._cats = [];
                let cat : RCat = new RCat();
                cat.id = "cat 1";
                cat.model_id = "1";
                cat.model_icon_path = `MineRCat/Sprites/RCatIcon/cat1`;
                cat.model_path = `MineRCat/Prefabs/Rcat1`;
                cat.exp = 0;
                cat.next_level_exp = 3000; 
                cat.dexterity = 150;
                cat.strength = 700;
                cat.constitution = 20;
                cat.intelligence = 10;
                let cat2 : RCat = new RCat();
                cat2.id = "cat2";
                cat2.model_id = "2";
                cat2.model_path = `MineRCat/Prefabs/Rcat2`;
                cat2.model_icon_path = `MineRCat/Sprites/RCatIcon/cat2`;
                cat2.exp = 0;
                cat2.next_level_exp = 3000; 
                cat2.dexterity = 190;
                cat2.strength = 700;
                cat2.constitution = 20;
                cat2.intelligence = 10;
                let cats : RCat[] = [];
                cats.push(cat);
                cats.push(cat2);
                resolve(cats);
            }
            catch(err){
                reject(err);
            }
        });
    }


    public setCatController(controllers : RCatController[]){
        this._catControllers = controllers;
        this._catControllers.forEach(catController   => {
            
        });
    }


    public generateMaterial(){
        return new Promise((resolve, reject) =>{
            
        });
    }

    public onUpdate(dt : number){
        if(!this._catControllers){
            return;
        }
        for(let controllerIndex = 0 ; controllerIndex < this._catControllers.length ; controllerIndex++){
            this._catControllers[controllerIndex].onUpdate(dt);
        }
    }
  
}


