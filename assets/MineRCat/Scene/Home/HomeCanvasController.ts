import { _decorator, Component, Node, instantiate, Prefab, input, Input, EventKeyboard } from 'cc';
import { GameDirector } from '../../../Gamerical/Utility/GameDirector';
import { RCat } from '../../Network/RCat';
import { RCatController } from '../../RCatController';
const { ccclass, property } = _decorator;

@ccclass('HomeCanvasController')
export class HomeCanvasController extends Component {
    _catControllers : RCatController[];


    protected start(): void {
        input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    protected update(dt: number): void {
        
    }

    private onKeyDown(event: EventKeyboard) : void{
        console.log(`Key down ${event}`);
    }

}


