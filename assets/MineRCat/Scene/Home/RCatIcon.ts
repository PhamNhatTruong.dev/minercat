import { _decorator, Component, Node, resources, Sprite, SpriteFrame, Input } from 'cc';
import { RCat } from '../../Network/RCat';
const { ccclass, property } = _decorator;

@ccclass('RCatIcon')
export class RCatIcon extends Component {
    @property({ type: Node, visible: true })
    private _selectedField: Node;

    private _sprite: Sprite = null;
    private _catData: RCat = null;
    private _isSelected: boolean = false;

    public set isSelected(value: boolean) {
        this._isSelected = value;
        this._selectedField.active = this._isSelected;
    }

    public onSelected: ((catIcon: RCatIcon) => void) | undefined;

    public get catData(): RCat {
        return this._catData;
    }
    public set catData(value: RCat) {
        this._catData = value;
        if (this._catData && this._catData.model_icon_path) {
            //CY TEST not load sprite
            //this.loadSpriteFrame(this._catData.model_icon_path);
        }
    }

    start() {
        this._sprite = this.getComponent(Sprite);
        this.node.on(Input.EventType.TOUCH_START, this.onButtonDown, this);
        this.isSelected = false;
    }

    private onButtonDown() {
        if (this.onSelected) {
            this.onSelected(this);
        }
    }

    protected onDestroy(): void {
        this.node.off(Input.EventType.TOUCH_START, this.onButtonDown, this);
    }

    private loadSpriteFrame(path: string) {
        resources.load(path, SpriteFrame, (err, spriteFrame) => {
            if (err) {
                console.error(`Failed to load SpriteFrame: ${err}`);
                return;
            }
            this.setSpriteFrame(spriteFrame);
        });
    }

    private setSpriteFrame(spriteFrame: SpriteFrame) {
        if (this._sprite) {
            this._sprite.spriteFrame = spriteFrame;
        } else {
            console.error('Sprite component is not assigned!');
        }
    }
}
