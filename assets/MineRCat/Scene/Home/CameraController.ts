import { _decorator, CCFloat, Component, EventTouch, Input, Node, UITransform, Vec2 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('CameraController')
export class CameraController extends Component {

    @property({
        type : CCFloat,
        visible : true
    })
    _touchSpeed : number;


    @property({
        type : Node,
        visible : true
    })
    _cameraBase : Node;

    @property({
        type : Node,
        visible : true
    })
    _canvasBase : Node;

    @property({
        type : Node,
        visible : true
    })
    _moveableField : Node;

    private _touchMoved : boolean;
    private _sceneRect : Vec2;
    private _moveableRect : Vec2;

    protected onLoad(): void {
        this.node.on(Input.EventType.TOUCH_START,this.onTouchStart,this,true);
        this.node.on(Input.EventType.TOUCH_END,this.onTouchEnd,this,true);
        this.node.on(Input.EventType.TOUCH_MOVE,this.onTouchMove,this,true);
        this.node.on(Input.EventType.TOUCH_CANCEL,this.onTouchCancel,this,true);
    }

    start() {
        let mableCanvas = this._moveableField.getComponent(UITransform);
        this._moveableRect = new Vec2(mableCanvas.width,mableCanvas.height);
        let canvas = this._canvasBase.getComponent(UITransform);
        this._sceneRect = new Vec2(canvas.width,canvas.height);
    }

    update(deltaTime: number) {

    }

    protected onDestroy(): void {
        this.node.off(Input.EventType.TOUCH_START,this.onTouchStart,this,true);
        this.node.off(Input.EventType.TOUCH_END,this.onTouchEnd,this,true);
        this.node.off(Input.EventType.TOUCH_MOVE,this.onTouchMove,this,true);
        this.node.off(Input.EventType.TOUCH_CANCEL,this.onTouchCancel,this,true);
    }

    onTouchMove(touchMove : EventTouch){
        let touches = touchMove.getTouches();
            
        let touch1 = touches[0];
        let delta1 = touch1.getDelta();
        let cam_pos = this._cameraBase.getPosition();

        cam_pos.x = cam_pos.x - delta1.x * this._touchSpeed;
        cam_pos.y = cam_pos.y - delta1.y * this._touchSpeed;
        if(Math.abs(cam_pos.x) > (this._moveableRect.x - this._sceneRect.x ) /2 ||
            Math.abs(cam_pos.y) > (this._moveableRect.y - this._sceneRect.y ) /2 ){
        }else{
            this._cameraBase.setPosition(cam_pos);
        }

        this.cancelInterTouch(touchMove);
        this.stopPropagationIfTargetIsMe(touchMove);
    }

    onTouchStart(touchStart : EventTouch){
        this._touchMoved = false;
        this.stopPropagationIfTargetIsMe(touchStart);
    }

    onTouchEnd(touchEnd ){
        if (this._touchMoved) {
            // touchEnd.stopPropagation();
        } else {
            this.stopPropagationIfTargetIsMe(touchEnd);
        }
     }

    onTouchCancel(touchCancel : EventTouch){
        this.stopPropagationIfTargetIsMe(touchCancel);
    }

    stopPropagationIfTargetIsMe(event){
        if(event.eventPhase ===  Event.AT_TARGET && event.target === this.node) {
            // event.stopPropagation();
        }
    }
    
    cancelInterTouch(event){
        var touch = event.touch;
        let p1 : Vec2 = touch.getLocation();
        let deltaMove : Vec2 = p1.subtract(touch.getStartLocation());
        if (deltaMove.length() > 7) {
            if (!this._touchMoved && event.target !== this.node) {
                // Simulate touch cancel for target node
                console.log(`cancelInterTouch`);
                var cancelEvent = new  EventTouch(event.getTouches(), event.bubbles,null,null);
                cancelEvent.type =  Node.EventType.TOUCH_CANCEL;
                cancelEvent.touch = event.touch;
                cancelEvent.simulate = true;
                event.target.dispatchEvent(cancelEvent);
                this._touchMoved = true;
            }
        }
    }
}


