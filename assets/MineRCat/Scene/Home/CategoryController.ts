import { _decorator, CCBoolean, Component, instantiate, Node, Prefab } from 'cc';
import { RCat } from '../../Network/RCat';
import { RCatIcon } from './RCatIcon';
const { ccclass, property } = _decorator;

@ccclass('CategoryController')
export class CategoryController extends Component {

    @property({type: Node, visible : true})
    private _contentsNode : Node;
    @property({ type: CCBoolean, visible: true })
    private _isMultiSelection: boolean;
    @property({type : Prefab, visible : true})
    private _rcatIconPrefab : Prefab;

    private _rcats: RCat[];
    private _rcatsIcons : RCatIcon[] = [];

    public get rcats(): RCat[] {
        return this._rcats;
    }
    public set rcats(value: RCat[]) {
        this._rcats = value;
        this.UpdateRcatIcon();
    }
 
    public onSelectedRCat: ((rcat: RCat) => void);

    public selectRCat(index : number){
        if(this._rcats){
            this._rcatsIcons[index].isSelected = true;
            this.onSelectedRCat(this._rcats[0]);
        }
    }

    private UpdateRcatIcon(){   
        console.log(`Start update Rcat category icon ${this._rcats.length}`)
        //clear old icons first
        if(this._rcatsIcons){
            this._rcatsIcons.forEach(element => {
                element.destroy();
            });
            this._rcatsIcons = [];
        }

        //generate icon
        this._rcats.forEach(catData => {
            try{
                console.log(`Generate rcat icon ${catData.model_icon_path}`)
                var catNode : Node = instantiate(this._rcatIconPrefab);
                catNode.setParent(this._contentsNode);
                let catController = catNode.getComponent(RCatIcon);
                catController.catData = catData;
                this._rcatsIcons.push(catController);
                catController.onSelected = this.onRCatSeleted.bind(this);
            }
            catch(err){
                console.error(err);
            }

        });
    }
    
    private onRCatSeleted(sender : RCatIcon){
        if(!this._isMultiSelection){
            this._rcatsIcons.forEach(element => {
                element.isSelected = false;
            });
        }
        sender.isSelected = true;
        if( this.onSelectedRCat){
            this.onSelectedRCat(sender.catData);
        }
    }
}


