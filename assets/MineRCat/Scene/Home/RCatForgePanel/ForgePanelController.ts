import { _decorator, CCInteger, CCString, Component, Enum, ImageAsset, instantiate, JsonAsset, Node, Prefab, resources, Sprite, SpriteFrame, Texture2D } from 'cc';
import { MaterialType } from './MaterialType';
import { ForgePanelConfig } from './ForgePanelConfig';
import { ForgeOptionController } from './ForgeOptionController';

const { ccclass, property } = _decorator;

@ccclass('ForgeMaterial')
export class ForgeMaterial {
    @property({type : CCInteger})
    amount : number;
    @property({type : Enum(MaterialType)})
    type : MaterialType;
}

@ccclass('ForgeFormula')
export class ForgeFormula{
    @property({type : CCString})
    name : string;
    @property({type : ForgeMaterial})
    source : ForgeMaterial[] = []
    @property({type : ForgeMaterial})
    finishedProduct : ForgeMaterial[] = []
}

@ccclass('ForgePanelController')
export class ForgePanelController extends Component {
    @property({type : CCString})
    assetPath : string;
    @property({type : ForgeFormula})
    fomular : ForgeFormula[] = []
    @property({type : Prefab})
    forgeOptionPrefab : Prefab;
    @property({type : Node})
    forgeOptionParent : Node;
    @property({type : Prefab})
    forgeMaterial : Prefab;
    @property({type : Node})
    sourceParent : Node;
    @property({type : Node})
    productParent : Node;

    private _spriteMap = new Map<MaterialType,SpriteFrame>;
    private _config : ForgePanelConfig;
    private _seletedFormula : ForgeFormula;

    public forgeFormula: ((updatePos : ForgeFormula) => void) | undefined;

    protected onLoad(): void {

    }

    start() {
        console.log(`------------start load resource path`);
        //start load resource path
        this.doLoadConfig().then( value => {
            return this.doLoadSprites();
        }).then(log => {
            return this.doLoadReforgeUI();
        }).then(log =>{
            console.log(`->>> Loaded log`);
            console.log(log);
            this.setReforgeOption(this.fomular[0]);
            this._seletedFormula = this.fomular[0];
        });    
    }

    private doLoadConfig(){
        return new Promise((resolve, reject) => {
            resources.load(this.assetPath,(err,data) => {
                if (err) {
                    console.error(`Failed to load ${this.assetPath}`, err);
                    reject("Cannot load sprite config");
                }            
                this._config =  (data as JsonAsset ).json as ForgePanelConfig
                resolve(this._config);    
            });
        });
    }

    private doLoadSprites(){
        return new Promise((resolve, reject) => {
            let resourceCount = this._config.sprites.length;
            let loadedSprite = 0;
            this._config.sprites.forEach(element => {
                if(!this._spriteMap.has(element.type_index)){
                    resources.load(element.image_path,ImageAsset,(err,imageAsset) => {
                        this._spriteMap.set(element.type_index, SpriteFrame.createWithImage(imageAsset));
                        loadedSprite++;
                        if(loadedSprite >= resourceCount){
                            resolve(`Load sprites done`);
                        }
                    });
                }else{
                    loadedSprite++; 
                }
            });
        });
    }

    private doLoadReforgeUI(){
        return new Promise((resolve, reject) => {
            try{
                this.fomular.forEach(forgeFormula => {
                    let instance =  instantiate(this.forgeOptionPrefab);
                    instance.setParent(this.forgeOptionParent);
                    let optionController = instance.getComponent(ForgeOptionController);
                    optionController.formula = forgeFormula;
                    optionController.onSelectedFormula = this.onFormularSeleted.bind(this);
                    let sprite = instance.getComponent(Sprite);
                    sprite.spriteFrame = this._spriteMap.get(forgeFormula.finishedProduct[0].type);                    
                });
                resolve(`Load completed`);
            }catch(err){
                console.log(err);
                reject(err);
            }
        });
    }
    
    private onFormularSeleted(formula: ForgeFormula) {
        console.log(`on seleted formular ${formula.source} => ${formula.finishedProduct}`);
        this._seletedFormula = formula;
        this.setReforgeOption(formula);    
    }

    private setReforgeOption(forgeFormula : ForgeFormula){
        //clear all current formular
        this.sourceParent.removeAllChildren();
        this.productParent.removeAllChildren();
        //insert
        forgeFormula.source.forEach(fmat => {
            let instance = instantiate(this.forgeMaterial);
            instance.setParent(this.sourceParent);
            instance.getComponent(Sprite).spriteFrame = this._spriteMap.get(fmat.type);
        });

        forgeFormula.finishedProduct.forEach(fmat => {
            let instance = instantiate(this.forgeMaterial);
            instance.setParent(this.productParent);
            instance.getComponent(Sprite).spriteFrame = this._spriteMap.get(fmat.type);   
        });
    }

    private onForgeButtonDown(){
        this.forgeFormula(this._seletedFormula);
    }

    update(deltaTime: number) {
        
    }


}


