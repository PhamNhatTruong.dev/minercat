import { MaterialType } from "./MaterialType";

export interface ForgePanelConfig {
    sprites: SpritePath[];
}

export interface SpritePath {
    type:       MaterialType;
    type_index : number;
    image_path: string;
}
