import { _decorator, CCString, Component, Node, Sprite } from 'cc';
import { ForgeFormula } from './ForgePanelController';
const { ccclass, property } = _decorator;

@ccclass('ForgeOptionController')
export class ForgeOptionController extends Component {
    
    public onSelectedFormula: ((formula: ForgeFormula) => void);

    @property({type : CCString, readonly : true})
    private formularName : string;
    
    private _formula: ForgeFormula;

    public get formula(): ForgeFormula {
        return this._formula;
    }
    public set formula(value: ForgeFormula) {
        this._formula = value;
        this.formularName = this._formula.name;
    }
    
    public OnSelected(){
        this.onSelectedFormula(this._formula);
    }
    
}


