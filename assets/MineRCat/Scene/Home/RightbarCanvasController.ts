import { _decorator, Component, Node, Animation, Input, AnimationComponent, animation, CCString } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('RightbarCanvasController')
export class RightbarCanvasController extends Component {

    @property({
        type : Node,
        visible : true
    })
    _triggerButton : Node;

    @property({
        type : CCString,
        visible : true
    })
    _triggerName : string;



    private _animationController: animation.AnimationController | null = null;
    private _conditionMet: boolean = false;

    start() {
        this._animationController = this.node.getComponent(animation.AnimationController);
        this._triggerButton.on(Input.EventType.TOUCH_START, () =>{
            this._animationController.setValue(this._triggerName,true);
        });
    }

    update(deltaTime: number) {
        
    }
}


