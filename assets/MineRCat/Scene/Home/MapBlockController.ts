import { _decorator, CCInteger, CCString, Component, Enum, Label, math, Node, Rect, Sprite, SpriteFrame, Vec2, Vec3 } from 'cc';
import { GameDirector } from '../../../Gamerical/Utility/GameDirector';
const { ccclass, property } = _decorator;


@ccclass('BlockSpriteMap')
export class BlockSpriteMap{
    @property({type : Vec2})
    mapPosition : Vec2 = new Vec2();
    @property({type : CCInteger})
    rotation : number;    
    @property({
        type: [CCString],
        tooltip: "A 3x3 grid of booleans representing the selectable grid",
    })
    grid: string[] = Array(3).fill("OOO");
}
 
@ccclass('MapBlockController')
export class MapBlockController extends Component {
    @property({type : SpriteFrame})
    spriteFrame : SpriteFrame;
    @property({type : Vec2})
    size : Vec2;
    @property({type : CCString ,
        readonly : true
    })
    debug : string = "";
    @property({type :  [BlockSpriteMap]})
    mapBlocks :  Array<BlockSpriteMap> = [];


    public startLoad(adjacentNode : number[][]){
        let middleNode = adjacentNode[1][1];
        for(let blockMapIndex = 0 ; blockMapIndex < this.mapBlocks.length ; blockMapIndex++){
            let blockMap = this.mapBlocks[blockMapIndex];
            let isChosen = true;
            for(let posY = 0 ; posY < adjacentNode.length ; posY ++){
                for(let posX = 0; posX < adjacentNode[0].length ; posX ++){
                    if(blockMap.grid[posY][posX] == "X" && adjacentNode[posY][posX] != middleNode){
                        isChosen = false;  
                        break;        
                    }
                    if(blockMap.grid[posY][posX] == "O" && adjacentNode[posY][posX] == middleNode){
                        isChosen = false;     
                        break;     
                    }
                    if(blockMap.grid[posY][posX] == "N"){
                        //just skip this block
                    }        
                }
            }
            if(isChosen){
                this.debug = blockMapIndex.toString(); 
                this.takeFrame(blockMap.mapPosition,blockMap.rotation);
                break;
            }
        }
        this.destroy();
     
    }

    private takeFrame( position : Vec2, rotation : number){
        const croppedSpriteFrame = new SpriteFrame();
        croppedSpriteFrame.texture = this.spriteFrame.texture;
        croppedSpriteFrame.rect = new Rect(
            position.x / this.size.x * this.spriteFrame.width,
            position.y / this.size.y * this.spriteFrame.height,
            this.spriteFrame.width / this.size.x,
            this.spriteFrame.width / this.size.y 
        )
        this.node.eulerAngles = new math.Vec3(0, 0, rotation);
        this.node.getComponent(Sprite).spriteFrame = croppedSpriteFrame;
    }

}


