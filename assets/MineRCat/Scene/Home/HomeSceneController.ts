import { _decorator, Component, instantiate, Layout, Node, Prefab, resources } from 'cc';
import { GamercialSceneController } from '../../../Gamerical/SceneManager/GamercialSceneController';
import { HomeLogic } from './HomeLogic';
import { HomeCanvasController } from './HomeCanvasController';
import { GameDirector } from '../../../Gamerical/Utility/GameDirector';
import { RCat } from '../../Network/RCat';
import { RCatController } from '../../RCatController';
import { MapBlockType } from '../../../Gamerical/TerrainManager/MapBlockType';
import { CategoryController } from './CategoryController';
import { RCatInfoPanelController } from './RCatPanel/RCatInfoPanelController';
import { UpdatePanelController } from './UpdatePanel/UpdatePanelController';
import { ForgeFormula, ForgePanelController } from './RCatForgePanel/ForgePanelController';
import { MapBlockController } from './MapBlockController';
const { ccclass, property } = _decorator;

@ccclass('MapSetting')
export class MapSetting{
    @property({type : Node })
    terrainParent : Node;
    @property({type : Node})
    rcatParent : Node;
}

@ccclass('RCatInfoPanel')
export class RCatInfoPanel{
    @property({type : RCatInfoPanelController})
    rcatInfoPanelController : RCatInfoPanelController;
}

@ccclass('RCatUpdatePanel')
export class RCatUpdatePanel{
    @property({type : UpdatePanelController})
    rcatUpdatePanelController : UpdatePanelController;
}

@ccclass(`RCatForgePanel`)
export class RCatForgePanel{
    @property({type : ForgePanelController})
    rcatForgePanelController : ForgePanelController;
}

@ccclass('HomeSceneController')
export class HomeSceneController extends GamercialSceneController {
 
    @property(MapSetting)
    private mapSetting = new MapSetting();
    @property(RCatInfoPanel)
    private rcatInfoPannel = new RCatInfoPanel();
    @property(RCatUpdatePanel)
    private rCatUpdatePanel = new RCatUpdatePanel();
    @property(RCatForgePanel)
    private rCatForgePanel = new RCatForgePanel();
    @property(HomeCanvasController)
    private rCatCanvasController;


    private _catControllers : RCatController[] = [];
    private _logic : HomeLogic;
    private _canvas : HomeCanvasController;
    private _map : number[][];
    private _width : number;
    private _height : number;
    private _cats : RCat[];
    private _loadingCompleted : boolean = false;
    private _blocks : Node[][]  = [];
    //#region GamercialSceneController implement

    protected start(): void {
        
    }

    public override onSceneLoaded() : void {
        console.log(`HomeSceneController on scene loaded`);
        this._logic = new HomeLogic();
        this._canvas = this.rCatCanvasController;
        if(!this._canvas){
            console.log(`Cannot found canvas`);
        }
        //Init map block prefab first
        //Generate map
        GameDirector.TerrainManager().initPrefab()
        .then(value => {
            return this._logic.generateMap();
        })
        //Show map
        .then(map => {
            console.log(`_logic.generateMap`);
            this._map = map as number[][];
            this._width = this._map[0].length;
            this._height = this._map.length;
            this.showMap(this._map,this._width,this._height);
            return this._logic.generateRCat();
        })
        //Get Rcat data
        .then (value => {
            let cats : RCat[] = value as RCat[];
            console.log(`Load ${cats.length} RCat`);
            this._cats = cats;
            this.rcatInfoPannel.rcatInfoPanelController.rcats = cats;
            this.rCatUpdatePanel.rcatUpdatePanelController.rcats = cats;
            //load rcat icon
            return this.loadRcatModel(cats);
        })
        //Load Rcat model
        .then (catsModel => {
            let catMap : Map<string,Prefab> = catsModel as Map<string,Prefab>;
            for(let catIndex = 0 ; catIndex < this._cats.length ; catIndex++ ){
                this.createRCate(this._cats[catIndex],catMap.get(this._cats[catIndex].model_id));
            }
            this.rcatInfoPannel.rcatInfoPanelController.catPrefabMap = catMap;
            this.rCatUpdatePanel.rcatUpdatePanelController.catPrefabMap = catMap;
            this._logic.setCatController(this._catControllers);
            this._loadingCompleted = true;
        //unactive layout component for update map
            this.mapSetting.terrainParent.getComponent(Layout).enabled = false;
        });

        GameDirector.TerrainManager().onUpdateResourceMap = this.onUpdateResourceMap.bind(this);
        this.rCatForgePanel.rcatForgePanelController.forgeFormula  = this.onForgeMaterial.bind(this);
    }

    public override onSceneUnLoad(): void {     
    }

    public override onAssetLoaded(): void {
    }

    public override onAssetUnload(): void{
        
    }
   
    //#endregion
     
    public showMap(map : number[][], width : number, height : number){
        this.mapSetting.terrainParent.removeAllChildren();
        console.log(`gen map`);
        for(let j = 0 ; j < height ; j++){
            let row : Node[] = [];
            for(let i = 0 ; i < width ; i++){
                let val = map[j][i];
                var node : Node = GameDirector.TerrainManager().getBlockPrefab(val);
                var instance = instantiate(node);
                instance.name = `Cell [${j},${i}]`;
                instance.setParent(this.mapSetting.terrainParent);
                row.push(instance);

                if(j > 1 && i > 1 && j < height -1 && i < width -1 && j < height- 1){
                    let blockctr = instance.getComponent(MapBlockController);
                    if(blockctr){   
                        let adjacentNode = [
                            [map[j-1][i-1],map[j-1][i],map[j-1][i+1]],
                            [map[j  ][i-1],map[j  ][i],map[j  ][i+1]],
                            [map[j+1][i-1],map[j+1][i],map[j+1][i+1]],
                        ]
                        blockctr.startLoad(adjacentNode);
                    }
                }
            }
            this._blocks.push(row);
        }
        
    }

    public createRCate(cat :RCat, model : Prefab){
        console.log(`gen rcat ${cat.id}`);
        if(!model){
           console.log(`not load prefab`);
           return; 
        }
        var instance = instantiate(model);
        instance.setParent(this.mapSetting.rcatParent);
        var catController = instance.getComponent(RCatController);
        catController.data = cat;
        this._catControllers.push(catController);
    }
    
    private loadRcatModel(cats : RCat[]){
        return new Promise( (resolve, reject) => {
            try{
                let modelMap : Map<string,Prefab> = new Map(); 
                let loadedModel = 0;
                for(let catIndex = 0 ; catIndex < cats.length ; catIndex++){
                    let path = cats[catIndex].model_path;
                    let id = cats[catIndex].model_id;
                    resources.load(path,Prefab,(err,catPrefab) => {
                        if(err){
                            console.log(`Cannot load rcat model ${cats[catIndex].model_path}`);
                        }else{
                            modelMap.set(id,catPrefab);
                        }
                        loadedModel ++;
                        if(loadedModel == cats.length){
                            resolve(modelMap);  
                        }
                    })
                }
            }catch(err){
                reject(err);
            }
        });
    }

    private onUpdateResourceMap(updatePos : [number,number], updateSource: MapBlockType){
        let block = this._blocks[updatePos[1]][updatePos[0]];
        let pos = block.position;
        let name = block.name;
        block.destroy();
        var newNode : Node = GameDirector.TerrainManager().getBlockPrefab(updateSource);
        var instance = instantiate(newNode);
        instance.name = name;
        instance.setParent(this.mapSetting.terrainParent);    
        instance.position = pos;    
        this._blocks[updatePos[1]][updatePos[0]] = newNode;
    }

    private onForgeMaterial(fomular : ForgeFormula){
        console.log(`Forge formula ${fomular}`);
    }


    protected update(dt: number): void {
        if(!this._loadingCompleted)
            return;
        this._logic.onUpdate(dt);
    }

    private Test(){

    }


}


