import { _decorator, Component, instantiate, Node, Prefab } from 'cc';
import { CategoryController } from '../CategoryController';
import { RCat } from '../../../Network/RCat';
const { ccclass, property } = _decorator;

@ccclass('RCatInfoPanelController')
export class RCatInfoPanelController extends Component {
    @property({type : CategoryController})
    private category : CategoryController;
    @property({type : Node})
    private catModelParent : Node;

    private _rcats: RCat[];
    private _rcatPrefabMap: Map<string, Prefab>;

    public get catPrefabMap(): Map<string, Prefab> {
        return this._rcatPrefabMap;
    }
    public set catPrefabMap(value: Map<string, Prefab>) {
        this._rcatPrefabMap = value;
        console.log(this._rcatPrefabMap);
        this.onselectedRCat(this._rcats[0]);
    }

    public get rcats(): RCat[] {
        return this._rcats;
    }
    public set rcats(value: RCat[]) {
        this._rcats = value;
        this.category.rcats = this._rcats;
    }

    start() {
        this.category.onSelectedRCat = this.onselectedRCat.bind(this);
    }

    private onselectedRCat(rCat : RCat){
        this.catModelParent.removeAllChildren();
        let model = instantiate(this._rcatPrefabMap.get(rCat.model_id));
        model.setParent(this.catModelParent);
    }
 
}


