import { _decorator, Component, Node,Label, Button, instantiate, Prefab } from 'cc';
import { CategoryController } from '../CategoryController';
import { RCat } from '../../../Network/RCat';
const { ccclass, property } = _decorator;

@ccclass('UpdatPanelController')
export class UpdatePanelController extends Component {
    @property({type : CategoryController})
    private category : CategoryController;
    @property({type : Node})
    private catModelParent : Node;
    @property({type : Label})
    private strengthVal : Label;
    @property({type : Label})
    private dexterityVal : Label;
    @property({type : Label})
    private consitutionVal : Label;
    @property({type : Label})
    private inteligentVal : Label;
    @property({type : Button})
    private updateButton : Button;
    @property({type : Label})
    private updateCostLb : Label;

    private _rcats: RCat[];
    private _catMap: Map<string, Prefab>;
    private _selectedCat : RCat;

    public get rcats(): RCat[] {
        return this._rcats;
    }
    public get catPrefabMap(): Map<string, Prefab> {
        return this._catMap;
    }
    public set catPrefabMap(value: Map<string, Prefab>) {
        this._catMap = value;
    }

    public set rcats(value: RCat[]) {
        this._rcats = value;
        this.category.rcats = this._rcats;
        this._selectedCat = this._rcats[0];
    }

    protected onLoad(): void {
        this.updateRcatStats(this._selectedCat);
    }

    start() {
        this.category.onSelectedRCat = this.onselectedRCat.bind(this);
        this.category.selectRCat(0);
    }
    
    private updateRcatStats(rCat : RCat){
        this.strengthVal.string = String(rCat.strength);
        this.dexterityVal.string = String(rCat.dexterity);
        this.consitutionVal.string = String(rCat.constitution);
        this.inteligentVal.string = String(rCat.intelligence);
    }

    private onselectedRCat(rCat : RCat){
        this.catModelParent.removeAllChildren();
        let model = instantiate(this._catMap.get(rCat.model_id));
        model.setParent(this.catModelParent);
        this._selectedCat = rCat;
        this.updateRcatStats(rCat);
    }

    private onUpdateButtonDown(){
        let selectedIndex = this._rcats.findIndex(cat => cat.id == this._selectedCat.id);
        if(selectedIndex == -1){
            console.log(`Cannot update Rcat`);
            return;
        }
        //Check if condition is meet
        this._selectedCat.strength += 10;
        this._selectedCat.dexterity += 10;
        this._selectedCat.constitution += 10;
        this._selectedCat.intelligence += 5;
        this._rcats[selectedIndex] = this._selectedCat;
        
        this.updateRcatStats(this._selectedCat);
    }

     
}


