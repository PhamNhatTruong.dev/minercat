import { _decorator, AudioSource, CCObject, Component, Input, input, Node } from 'cc';
import { LobbyLogic } from './LobbyLogic';
const { ccclass, property } = _decorator;

@ccclass('LobbyCanvasController')
export class LobbyCanvasController extends Component {

    @property({
        type : Node,
        visible : true
    })
    _tapToLoginField : Node;

    @property({
        type : Node,
        visible : true
    })
    _loginPannel : Node;

    @property({
        type : Node,
        visible : true
    })
    _loginButton : Node;
    
    start() {
        this.initScene();
    }

    public onLoginEvent: ((username : string, password : string) => void) | undefined;

    initScene(){
        this._tapToLoginField.on(Input.EventType.TOUCH_START, () => {
            this._loginPannel.active = true;
        },this);
        this._loginButton.on(Input.EventType.TOUCH_START,() => {
            this.onLoginEvent("Username","Password");
        },this)
    }
}


