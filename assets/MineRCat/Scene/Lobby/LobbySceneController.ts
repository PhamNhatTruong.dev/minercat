import { _decorator, Component, Node, settings  } from 'cc';
import { GamercialSceneController } from '../../../Gamerical/SceneManager/GamercialSceneController';
import { LobbyLogic } from './LobbyLogic';
import { LobbyCanvasController } from './LobbyCanvasController';
import { GameDirector } from '../../../Gamerical/Utility/GameDirector';
const { ccclass, property } = _decorator;

@ccclass('LobbySceneController')
export class LobbySceneController extends GamercialSceneController {

    private _logic : LobbyLogic;
    private _canvas : LobbyCanvasController;

    public override onSceneLoaded() : void {
        console.log(`LobbySceneController on scene loaded`);
        const server = settings.querySettings('assets', 'server');
        console.log(`Server ${server}`);
        this._logic = new LobbyLogic();
        this._canvas = this.node.parent.getComponentInChildren(LobbyCanvasController);
        if(!this._canvas){
            console.log(`Cannot found canvas`);
        }

        GameDirector.SoundManager().preloadSoundsClip("lobbyScene").then(result =>{
            GameDirector.SoundManager().playBgm("bgm 2");
        });

        this._canvas.onLoginEvent = this.onLogin.bind(this);
    }

    public override onSceneUnLoad(): void {
        
    }

    public override onAssetLoaded(): void {
        
    }

    public override onAssetUnload(): void{
        
    }

    private onLogin(username : string, password : string) : void{
        console.log("LobbySceneController onLogin");
        this._logic.logIn(username,password, 
        () => {
            GameDirector.SceneManager().loadScene("Home");
        },
        (error : string) => {

        }
        );
    }
}


