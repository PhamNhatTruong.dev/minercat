import { _decorator, Component, Node, Button, CCFloat, Enum, Input } from 'cc';
const { ccclass, property } = _decorator;

export enum Key{
    BUTTON_DOWN,
    BUTTON_UP
}

export enum Action{
    ACTIVE,
    INACTIVE
}

@ccclass("ButtonEvent")
export class ButtonEvent {
    @property({type : Enum(Key)})
    key: Key = Key.BUTTON_DOWN;
    @property({type : Node})
    node : Node;
    @property({type : Enum(Action)})
    action: Action = Action.ACTIVE;
}

@ccclass('UiButtonEvents')
export class UiButtonEvents extends Component {
    @property(ButtonEvent) 
    protected buttonEvents: ButtonEvent[] = []; 

    start() {
        this.node.on(Input.EventType.TOUCH_START,this.handleMouseDown.bind(this));
        this.node.on(Input.EventType.TOUCH_END,this.handleMouseUp.bind(this));

    }

    private handleMouseDown() {
        this.buttonEvents.forEach(event => {
            if(event.key == Key.BUTTON_DOWN)
            event.node.active = event.action == Action.ACTIVE;
        });
    }

    private handleMouseUp(){
        this.buttonEvents.forEach(event => {
            if(event.key == Key.BUTTON_UP)
            event.node.active = event.action == Action.ACTIVE;
        });
    }

    protected onDestroy(): void {
        this.node.off(Input.EventType.TOUCH_START,this.handleMouseDown);
        this.node.off(Input.EventType.TOUCH_END,this.handleMouseUp);
    }
}


