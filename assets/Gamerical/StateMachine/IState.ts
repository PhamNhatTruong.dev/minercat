import StateMachine from "./StateMachine";

export interface IState {
  _name: string;
  _stateMachine: StateMachine;

  onEnter(): void;
  onUpdate(dt: number): void;
  onExit(): void;
}
