import { IState } from "./IState"
import { _decorator, CCInteger, CCString, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('StateMachine')

export default class StateMachine extends Component
{
	private _states = new Map<string, IState>()
	private _currentState?: IState

	@property({
		type : CCString,
		visible : true
	})
	_defaultStateName : string;

	@property({
		type : CCString,
		visible : true,
		readonly : true
	})
	_currentStateName : string;

	public addState(name: string, state : IState)
	{
		this._states[name] = state;
	}

	public setState(name: string)
	{
		if(this._currentState){
			// console.log(`Exit ${this._currentStateName} state`)
			this._currentState.onExit();
		}
		// console.log(`Start ${name} state`);
		this._currentStateName = name;
		this._currentState = this._states[name];
		this._currentState.onEnter();
	}

	public onUpdate(dt: number)
	{
		if(!this._currentState && this._states[this._defaultStateName]){
			this.setState(this._defaultStateName);
		}
		this._currentState.onUpdate(dt);
    }
}