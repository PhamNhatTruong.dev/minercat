// index by rule 
// ab 000000
// a: can interact, 1 mean yes
// b: can go through 1 mean yes
export enum MapBlockType{
    FLOOR = 64,   //01 000000 
    WALL = 128,   //10 000000
    BRICK = 129,  //10 000001
    STONE = 130,  //10 000010
    GOLD = 131,   //10 000011
    BASE = 1,     //00 000001
    LADDER = 193  //11 000001
}