  interface Noisemap {
    frequency: number;
    step: number;
  }
  interface Block {
    type_index: number;
    name:       string;
    path:       string;
    toughness_multiply: number;
}
  interface TerrainManagerConfig {
    width: number;
    height: number;
    home_size : number;
    boundary : number;
    wall_limit : number;
    noise_map: Noisemap;
    blocks: Block[];
  }
   
