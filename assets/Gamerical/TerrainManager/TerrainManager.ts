import { _decorator, Component, Node, Prefab, resources, Vec2 } from 'cc';
import { MonoSingleton } from '../Utility/MonoSingleton';
import { TerrainManagerComponent } from './TerrainManagerComponent';
import { MapBlockType } from './MapBlockType';
const { ccclass, property } = _decorator;

const blockDictionary: { [key: number]: any } = {};
@ccclass('TerrainManager')
export class TerrainManager extends MonoSingleton<TerrainManagerComponent,TerrainManagerConfig> {

    
    private _terrainMap: number[][];
    private _resourceMap: number[][];
    private _toughnessMap: number[][];
   
    public get resourceMap(): number[][] {
        return this._resourceMap;
    }
    public set resourceMap(value: number[][]) {
        this._resourceMap = value;
        this.saveResource(this._resourceMap);
    }    
    public get terrainMap(): number[][] {
        return this._terrainMap;
    }
    public set terrainMap(value: number[][]) {
        this._terrainMap = value;
        this.saveMap(this._terrainMap);
    }
    public get toughnessMap(): number[][] {
        return this._toughnessMap;
    }
    public set toughnessMap(value: number[][]) {
        this._toughnessMap = value;
        this.saveToughness(this.toughnessMap);
    }
    public get mapWidth() : number{
        return this._config.width;
    }
    public get mapHeight() : number{
        return this._config.height;
    }
    public get baseSize() : number{
        return this._config.home_size;
    }

    public onUpdateResourceMap: ((updatePos : [number,number], updateSource: MapBlockType) => void) | undefined;

    //#region Terrain generate
    
    public randomFill() {
        return new Promise((resolve, reject) =>{
            let noiseMap : number[][] = [];
            const randomColumn = Math.floor(Math.random() * (this._config.width - 8)) + 4;
            for (let y = 0; y < this._config.height; y++) {
                let row : number[] = []
                for (let x = 0; x < this._config.width; x++) {
                    if (x === 0 || y === 0 || x === this._config.width - 1 || y === this._config.height - 1) {
                        row.push(MapBlockType.WALL);
                    }else if(Math.abs(randomColumn - x ) < 1){
                        //this will add random collumn to connect map
                        row.push(MapBlockType.FLOOR);
                    } 
                    else if (Math.random() * 100 > this._config.noise_map.frequency) {
                        row.push(MapBlockType.WALL);
                    } else {
                        row.push(MapBlockType.FLOOR);
                    }
                }
                noiseMap.push(row);
            }
            return resolve(noiseMap);
        })
      
    }
    public generateCellularAutomationMap(input : number[][])  {
        return new Promise((resolve, reject) => {
            for (let i = 0; i < this._config.noise_map.step; i++) {
                input = this.step(input);
            }
            resolve(input);
        });
    }
    private step(map: number[][]):number[][] {
        let width : number = this._config.width;
        let height : number = this._config.height;
        const tempMap: number[][] = [];
        for (let y = 0; y < height; y++) {
            let row : number[] = [];
            for (let x = 0; x < width; x++) {
                if (x === 0 || y === 0 || x === width - 1 || y === height - 1) {
                    row.push(MapBlockType.WALL);
                } else {
                    row.push(this.placeWallLogic(map, x, y) ? MapBlockType.WALL : MapBlockType.FLOOR );
                }
            }
            tempMap.push(row);
        }
        return tempMap;
    }
    private countAdjacentWalls(input : number[][], x : number , y : number) : number{
        let walls = 0;
        for (let mapY = y - 1; mapY <= y + 1; mapY++){
            for(let mapX = x - 1; mapX <= x + 1; mapX ++){
                if(!(mapX == x && mapY == y)){
                    if(input[mapY][mapX] == MapBlockType.WALL){
                        walls++;
                    }
                }
            }
        }
        return walls;
    }
    public USmoothMap(input : number[][]) : number[][]{
        let tempMap : number[][] = [];
        var leftSide : number[][] = [[0,1,1],[0,0,1],[0,1,1]];
        var rightSide : number[][] = [[1,1,0],[1,0,0],[1,1,0]];
        var topSide : number[][] = [[1,1,1],[1,0,1],[0,0,0]];
        var bottomSide : number[][] = [[0,0,0],[1,0,1],[1,1,1]];
        var checkSize = [leftSide,rightSide,topSide,bottomSide];
        for(let y = 0 ; y < this._config.height; y++){
            let row : number[] = []
            for (let x = 0; x < this._config.width ; x++) {
                if(x < this._config.boundary || x > this._config.width - this._config.boundary||
                    y < this._config.boundary || y > this._config.height - this._config.boundary){
                      row.push(MapBlockType.WALL);  
                      continue;
                }
                if(input[y][x] == MapBlockType.WALL){
                    row.push(MapBlockType.WALL);
                    for(let checkSideIndex = 0; checkSideIndex < checkSize.length ; checkSideIndex ++){
                        if(this.checkIsNotContainWallInFindMap(input,checkSize[checkSideIndex],x,y)){
                            row.pop();
                            row.push(MapBlockType.FLOOR);
                            break;
                        }
                    }
                }else{
                    row.push(MapBlockType.FLOOR);
                }
            }
            tempMap.push(row);
        }
        return tempMap;
    }
    private checkIsNotContainWallInFindMap(input : number[][], map : number[][], x : number, y : number) : boolean{
        for(var j = -1 ; j < 2 ; j ++){
            for(var i = -1 ; i < 2 ; i++){
                if(input[j + y][i + x] == MapBlockType.WALL && map[j + 1][i + 1] == 1){
                    return false;
                }
            }
        }
        return true;
    }
    private placeWallLogic(map: number[][],  x: number, y: number): boolean {
        // return this.countAdjacentWalls(map,x, y) >= this._config.wall_limit ||
        //        this.countNearByWalls(map, x, y) <= 2;
        return this.countAdjacentWalls(map,x, y) >= this._config.wall_limit || 
                x < this._config.boundary ||
                x > this._config.width - this._config.boundary ||
                y < this._config.boundary || 
                y > this._config.height - this._config.boundary ;
    }
    public putResources(map : number[][]){
        return new Promise((resolve, reject) => {
            let resourceMap : number[][] = [];
            let baselocationX : number = 0;
            let baselocationY : number = 0;

            //find base first
            for(let j = 0 ; j < map.length; j++){
                for(let i = 0 ; i < map[0].length ; i++){
                    if(map[j][i] == MapBlockType.BASE && baselocationX == 0){
                        baselocationX = i + this._config.home_size / 2;
                        baselocationY = j + this._config.home_size / 2;
                        break;
                    }
                }
                if(baselocationX != 0){
                    break;
                }
            }
            //start put resource 
            for(let j = 0 ; j < map.length; j++){
                let row : number[] = [];
                for(let i = 0 ; i < map[0].length ; i++){ 
                    if(map[j][i] == MapBlockType.FLOOR){
                        //just test indensity here
                        if(Math.pow(baselocationY - j,2) + Math.pow(baselocationX - i,2) < Math.pow(this._config.home_size * 1.5,2) ){
                            row.push(MapBlockType.FLOOR);
                        }else
                        if(Math.random() * 100 > 0){
                            row.push(MapBlockType.BRICK);
                        }else{
                            row.push(MapBlockType.FLOOR);
                        }
                    }else{
                        row.push(map[j][i]);
                    }
                }

                resourceMap.push(row);
            }    
            resolve(resourceMap);
        });
    }
    public createToughnessMap(sourceMap : number[][]){
        return new Promise((resolve, reject) => {
            try{
                let toughnessMap : number[][] = [];
                for(let j = 0 ; j < sourceMap.length ; j++){
                    let row : number[] = [];
                    for(let i = 0; i < sourceMap.length; i++){
                        //can not interact
                        if((sourceMap[j][i] & (1 << 7 )) == 0){
                            row.push(0);
                        }else{
                            //test just test val here
                            //in the future : must be calculate 
                            row.push(100);
                        }
                    }
                    toughnessMap.push(row);
                }
                resolve(toughnessMap);
            }catch(err){
                reject(err);
            }
        });
    }
    public putCampPlace(map : number[][]){
        return new Promise((resolve, reject) => {
            var [campX, campY] = this.findAvaiableArena(map);
            for(let j = campY - this._config.home_size; j < campY ; j++){
                for(let i = campX - this._config.home_size ; i < campX ; i++){
                    map[j][i] = MapBlockType.BASE;
                }
            }
            resolve(map);
        });
    }

    private findAvaiableArena(map : number[][]) : [number , number]  {
        let fMap: number[][] = [];
        for (let i = 0; i < map[0].length; i++) {
            fMap[0] ??= [];
            fMap[0][i] = 1;
        }
        for (let j = 0; j < map.length; j++) {
            fMap[j] ??= [];
            fMap[j][0] = 1;
        }
        for (let j = 2 ; j < map.length; j++){
            for(let i = 2 ; i < map.length ; i++){
                if(map[j][i] == map[j-1][i] && map[j][i] == map[j][i-1] && map[j-1][j] && map[j][i] != MapBlockType.WALL){
                    fMap[j][i] = Math.min(fMap[j-1][i],Math.min(fMap[j][i-1],fMap[j-1][i-1])) + 1;
                }else{
                    fMap[j][i] = 1;
                }
                if(fMap[j][i] > this._config.home_size){
                    console.log(`Put map in ${i} ${j}`);
                    return [i , j];
                }
            }
        }
        return [-1, -1];
    }


    //#endregion

    //#region save/load
    public saveMap(map : number[][]){
        let mapJson = JSON.stringify(map);
        localStorage.setItem("map",mapJson);
    }

    public saveResource(resourceMap : number[][]){
        let resourceJson = JSON.stringify(resourceMap);
        localStorage.setItem("resource",resourceJson);
    }

    public saveToughness(toughnessMap : number[][]){
        let toughnessJson = JSON.stringify(toughnessMap);
        localStorage.setItem("toughness",toughnessJson);  
    }

    public loadMap() {
        return new Promise( (resolve, reject) => {
            try{
                let mapJson = localStorage.getItem("map");
                if(mapJson){
                    let map = JSON.parse(mapJson);
                    resolve(map as number[][]);
                }else{
                    reject("cannot found map");
                }
            }catch(err) {
                reject(err);
            }    
        });
    }

    public loadResource(){
        return new Promise( (resolve , reject) => {
            try{
                let resourceJson = localStorage.getItem("resource");
                if(resourceJson){
                    let map = JSON.parse(resourceJson);
                    resolve(map as number[][]);
                }else{
                    reject("Cannot found resouce");
                }
            }catch(err){
                reject(err);
            }
        });
    }

    public loadToughness(){
        return new Promise( (resolve , reject) => {
            try{
                let resourceJson = localStorage.getItem("toughness");
                if(resourceJson){
                    let map = JSON.parse(resourceJson);
                    for(let j = 0; j < map.length; j++){
                        for(let i = 0; i < map[0].length; i++){
                            //clear exploiting block
                            map[j][i] = ( map[j][i] & 0x7F ); 
                        }
                    }
                    resolve(map as number[][]);
                }else{
                    reject("Cannot found toughness");
                }
            }catch(err){
                reject(err);
            }
        });
    }

    //#endregion

    //#region Block handler
    public initPrefab(){
        return new Promise((resolve,reject)=>{
            let resourceLoaded = 0;
            for(let blockPrefabIndex = 0 ; blockPrefabIndex < this._config.blocks.length ; blockPrefabIndex++){
                resources.load(this._config.blocks[blockPrefabIndex].path,Prefab,(err,blockPrefab) =>{
                    if(err){
                        console.error(`Cannot load prefab ${this._config.blocks[blockPrefabIndex].path} ${err}`);
                    }else{
                        console.log(`Load prefab ${this._config.blocks[blockPrefabIndex].name}`);
                        blockDictionary[this._config.blocks[blockPrefabIndex].type_index] = blockPrefab;
                    }
                    resourceLoaded++
                    if(resourceLoaded >=  this._config.blocks.length ){
                        resolve("Load all resouce completed");
                    }
                });
            }           
        });
        
    }

    public getBlockPrefab(type : MapBlockType) {
        var prefab = blockDictionary[type as number];
        if(!prefab){
            console.log(`Prefab not found`);
            return null;
        }
        return prefab;
    } 

    public clearResource( x : number, y: number) {
        this._resourceMap[y][x] = MapBlockType.FLOOR;
        this.saveResource(this._resourceMap);
        this._toughnessMap[y][x] = 0;
        this.saveToughness(this._toughnessMap);
        this.onUpdateResourceMap([x,y],MapBlockType.FLOOR);
    }
}


