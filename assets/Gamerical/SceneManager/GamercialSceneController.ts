import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('GamercialSceneController')
export class GamercialSceneController extends Component {
    public onSceneLoaded?():void;
    public onAssetLoaded?():void;
    public onSceneUnLoad?():void;
    public onAssetUnload?():void;
}


