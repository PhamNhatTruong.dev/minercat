import { _decorator, Component, Node,Prefab,resources } from 'cc';
import { ISystemBase } from '../Utility/ISystemBase';
import { SceneManagerComponent } from './SceneManagerComponent';
import { MonoSingleton } from '../Utility/MonoSingleton';
import { GamercialSceneController } from './GamercialSceneController';
const { ccclass, property } = _decorator;

@ccclass('SceneManager')
export class SceneManager  extends MonoSingleton<SceneManagerComponent,SceneManagerConfg> {

    private _sceneStack : string[] ;
    private _currentSceneController : GamercialSceneController;

    public loadFirstScene() : void{
        this._sceneStack = [];
        let start_scene : string = this._config.start_scene;
        this.pushSceneWithName(start_scene,null);
    }

    public loadScene(name : string) : void  {
        this.pushSceneWithName(name);
    }

    public pushSceneWithName( name : string, data : any = null){
        let sceneConfig = this._config.scene.find(scene => scene.name === name);
        let path = sceneConfig.path;
        if(!path){
            console.log(`Cannot load scene ${name}: not found`);
            return;
        }
        if(this._currentSceneController){
            this._currentSceneController.onSceneUnLoad();
        }
        
        if(sceneConfig.preloading_assets.length > 0){
            for(let pathIndex = 0 ; pathIndex < sceneConfig.preloading_assets.length ; pathIndex++){
                if(sceneConfig.preloading_assets[pathIndex].asset_name == "prefab"){
                    resources.preload(sceneConfig.preloading_assets[pathIndex].asset_path,Prefab);
                }
                //add more type later
            }
        }

        this._component.loadScene(path, () => {
            //must sure scene is loaded
            console.log(this._component.node);
            this._currentSceneController =  this._component.node.parent.getComponentInChildren(GamercialSceneController);
            if(!this._currentSceneController){
                console.log(`Cannot get Scene controller`);
                return;
            }
            this._currentSceneController.onSceneLoaded();
        });
      
    }

    public pushSceneWithIndex( sceneNumber : number, data : any = null){
        let path = this._config.scene.find(scene => scene.index === sceneNumber).path;
        if(!path){
            console.log(`Cannot load scene ${sceneNumber}: not found`);
            return;
        }
        console.log("Not implement");
     }
    
    private doPushScene( name : string, data : any = null){
        //some thing need to do in future 
        // 1 closing animation
        // preloading assets
        // unloading assets
        // show loading scene
    }

    private doPushSceneSync( name : string, data : any = null){
        
    }

}



