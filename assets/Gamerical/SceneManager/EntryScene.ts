import { _decorator, CCBoolean, CCString, Component, director, Node } from 'cc';
import { SceneManagerComponent } from './SceneManagerComponent';
import { GameDirector } from '../Utility/GameDirector';
import { SoundManagerComponent } from '../SoundManager/SoundManagerComponent';
import { TerrainManagerComponent } from '../TerrainManager/TerrainManagerComponent';
import { NetworkManagerComponent } from '../NetworkManager/NetworkManagerComponent';
const { ccclass, property } = _decorator;
// https://json2ts.dev/

@ccclass('EntryScene')
export class EntryScene extends Component {

    private _initializedCount : number = 0;
    private _assignedSystemCount : number = 0;

    @property({
        type : CCBoolean,
        visible : true  
    })
    _sceneManager : boolean;

    @property({
        visible : true
    })
    _sceneManagerConfigPath : string = "";

    
    @property({
        type : CCBoolean,
        visible : true
    })
    _soundManager : boolean;

    @property({
        visible : true
    })
    _soundManagerConfigPath : string = "";

    @property({
         type : CCBoolean,
         visible : true
    })
    _terrainManager : boolean ;

    @property({
        visible : true
    })
    _terrainManagerConfigPath : string = "";

    @property({
        type : CCBoolean,
        visible : true
    })
    _networkManager : true;

    @property({
        visible : true
    })
    _networkManagerConfigPaht : string = "";

    start() {
        this._initializedCount = 0;
        this._assignedSystemCount = 0;
        if(this._terrainManager){
            let terrainManagerNode = new Node("TerrainManager");
            this.node.parent.addChild(terrainManagerNode);
            director.addPersistRootNode(terrainManagerNode);
            const myComp = terrainManagerNode.addComponent(TerrainManagerComponent);
            const monoConfig : MonoSingletonConfig = {
                configPath : this._terrainManagerConfigPath
            }
            GameDirector.TerrainManager().initialize(monoConfig,this.initialized.bind(this));
            GameDirector.TerrainManager().setComponent(myComp);
            this._assignedSystemCount ++
        }
        if(this._sceneManager){
            let sceneManagerNode = new Node("SceneManager");
            this.node.parent.addChild(sceneManagerNode);
            director.addPersistRootNode(sceneManagerNode);
            const myComp = sceneManagerNode.addComponent(SceneManagerComponent);
            const monoConfig : MonoSingletonConfig = {
                configPath : this._sceneManagerConfigPath
            }
            GameDirector.SceneManager().initialize(monoConfig,this.initialized.bind(this));
            GameDirector.SceneManager().setComponent(myComp);
            this._assignedSystemCount ++
        }
        if(this._soundManager){
            let soundManagerNode = new Node("SoundManager");
            this.node.parent.addChild(soundManagerNode);
            director.addPersistRootNode(soundManagerNode);
            const myComp = soundManagerNode.addComponent(SoundManagerComponent);
            const monoConfig : MonoSingletonConfig = {
                configPath : this._soundManagerConfigPath
            }
            GameDirector.SoundManager().initialize(monoConfig,this.initialized.bind(this));
            GameDirector.SoundManager().setComponent(myComp);
            this._assignedSystemCount ++
        }
        if(this._networkManager){
            let networkManagerNode = new Node("NetworkManager");
            this.node.parent.addChild(networkManagerNode);
            director.addPersistRootNode(networkManagerNode);
            const myComp = networkManagerNode.addComponent(NetworkManagerComponent);
            const monoConfig : MonoSingletonConfig = {
                configPath : this._networkManagerConfigPaht
            }
            GameDirector.NetworkManager().initialize(monoConfig,this.initialized.bind(this));
            GameDirector.NetworkManager().setComponent(myComp);
            this._assignedSystemCount ++
        }

    }

    initialized = () => {
        this._initializedCount ++;
        console.log(`Initialized ${this._initializedCount}`)
        if(this._initializedCount >= this._assignedSystemCount){
            console.log("Start load first scene");
            GameDirector.SceneManager().loadFirstScene();
        }
    }

    update(deltaTime: number) {
       
    }
}


