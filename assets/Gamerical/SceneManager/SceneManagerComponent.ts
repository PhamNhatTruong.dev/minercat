import { _decorator, Component, director, error, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('SceneManagerComponent')
export class SceneManagerComponent extends Component {
    start() {
    }

    update(deltaTime: number) {
        
    }

    public loadFirstScene() : void {
        
    }  
    
    public loadScene(scenename : string, onSceneLoaded : ( ) => void ){
        director.loadScene(scenename,  () => {
            onSceneLoaded();
        });
    }
}


