export interface AsNode {
    x: number;
    y: number;
    g: number;
    h: number;
    f: number;
    parent?: AsNode;
} 