import { SceneManager } from '../SceneManager/SceneManager'
import { SoundManager } from '../SoundManager/SoundManager'
import { SceneManagerComponent } from '../SceneManager/SceneManagerComponent';
import { SoundManagerComponent } from '../SoundManager/SoundManagerComponent';
import { TerrainManager } from '../TerrainManager/TerrainManager';
import { TerrainManagerComponent } from '../TerrainManager/TerrainManagerComponent';
import { NetworkManager } from '../NetworkManager/NetworkManager';
import { NetworkManagerComponent } from '../NetworkManager/NetworkManagerComponent';
import { SoundManagerConfig } from '../SoundManager/SoundManagerConfig';

export class GameDirector{
  
    public static SceneManager() : SceneManager {
        return SceneManager.getInstance<SceneManagerComponent,SceneManagerConfg>("SceneManager") as SceneManager;
    }

    public static TerrainManager() : TerrainManager{
        return TerrainManager.getInstance<TerrainManagerComponent,TerrainManagerConfig>("TerrainManager") as TerrainManager;
    }

    public static SoundManager() : SoundManager {
        return SoundManager.getInstance<SoundManagerComponent,SoundManagerConfig>("SoundManager") as SoundManager;
    }

    public static NetworkManager() : NetworkManager{
        return NetworkManager.getInstance<NetworkManagerComponent,NetWorkManagerConfig>("NetworkManager") as NetworkManager;
    }
}


