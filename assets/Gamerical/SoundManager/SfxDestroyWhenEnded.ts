import { _decorator, AudioSource, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('SfxDestroyWhenEnded')
export class SfxDestroyWhenEnded extends Component {
    private _audioSource : AudioSource;
    start() {
        this._audioSource = this.node.getComponent(AudioSource);
        if(!this._audioSource){
            return;
        }
        this._audioSource.node.on("ended",this.onAudioEnded,this);
    }

    private onAudioEnded(){
        if(this._audioSource){
            this._audioSource.node.off("ended",this.onAudioEnded,this);
        }
        this.node.destroy();
    }
}


