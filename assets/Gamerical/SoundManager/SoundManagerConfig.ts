 export interface SoundManagerConfig {
    group: Group[];
}

export interface Group {
    name: string;
    bmg:  Bmg[];
    sfx:  Sfx[];
}

export interface Bmg {
    name:    string;
    dir:     string;
}

export interface Sfx {
    name: string;
    dir:  string;
}
