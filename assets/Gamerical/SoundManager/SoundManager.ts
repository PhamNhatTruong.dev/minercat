import { _decorator, AudioClip, AudioSource, Component, instantiate, Node, resources } from 'cc';
import { SoundManagerComponent } from './SoundManagerComponent';
import { MonoSingleton } from '../Utility/MonoSingleton';
import { SoundManagerConfig } from './SoundManagerConfig';
import { SfxDestroyWhenEnded } from './SfxDestroyWhenEnded';
const { ccclass, property } = _decorator;

@ccclass('SoundManager')
export class SoundManager extends MonoSingleton<SoundManagerComponent,SoundManagerConfig> {

    private _bgmAudioSource : AudioSource;
    private _sfxAudioSource : AudioSource[] = [];
    private _currentGroupName : string;
    private _soundClips : Map<string,AudioClip>;


    public preloadSoundsClip(groupName: string): Promise<void> {   
        return new Promise((resolve, reject) => {
            let soundLoaded = 0; 
            this._currentGroupName = groupName;
            
            const group = this._config.group.find(group => group.name == groupName);
            if (!group) {
                console.log(`Not found sound group ${groupName}`);
                reject(`Not found sound group ${groupName}`);
                return;
            }
            const targetSoundCount = group.bmg.length + group.sfx.length; // The number of sounds we expect to load before resolving

            this._soundClips = new Map<string, AudioClip>();
            
            const onSoundLoaded = () => {
                soundLoaded++;
                if (soundLoaded >= targetSoundCount) {
                    resolve();
                }
            };
            
            const loadSound = (path: string) => {
                if (this._soundClips.has(path)) {
                    onSoundLoaded();
                    return;
                }else{
                    
                }
                this.loadAudioClip(path).then(clip => {
                    console.log(`Loaded ${path}`);
                    this._soundClips.set(path, clip as AudioClip);
                    onSoundLoaded();
                }).catch(err => {
                    console.error(err);
                    onSoundLoaded();
                });
            };
            
            for (const bmg of group.bmg) {
                loadSound(bmg.dir);
            }
            
            for (const sfx of group.sfx) {
                loadSound(sfx.dir);
            }
            
        });
    }

    private loadAudioClip(path : string) {
        return new Promise((resolve, reject) => {
            resources.load(path,AudioClip , (err, clip) => {
                if(err){
                    console.log(`Cannot load sound ${path}`);
                    reject(err);
                }else{
                    resolve(clip); 
                }
            });
        });
    }

    public unloadSoundsClip(){
        this._soundClips.clear();
    }
 

    public playBgm(soundkey : string){
        if(!this._bgmAudioSource){
            const bgmNode = new Node(`Bgm`);
            bgmNode.setParent(this._component.node);
            this._bgmAudioSource = bgmNode.addComponent(AudioSource);
        }
        let foundGroup = this._config.group.filter(group => group.name == this._currentGroupName);
        if(!foundGroup){
            console.log(`Cannot found group`);
            return;
        }
        let sound =  foundGroup[0].bmg.filter(bgm => bgm.name == soundkey);
        if(!sound){
            console.log(`Cannot found sound ${soundkey}`);
            return;
        }
        let soundPath = sound[0].dir;
        if(this._soundClips.has(soundPath)){
            this._bgmAudioSource.clip = this._soundClips.get(soundPath);
            this._bgmAudioSource.loop = true;
            this._bgmAudioSource.play();
        }
    }
    


    public stopBgm(){
        if(this._bgmAudioSource){
            this._bgmAudioSource.stop();
        }
    }

    public playSfx(soundKey : string){
        let foundGroup = this._config.group.filter(group => group.name == this._currentGroupName);
        if(!foundGroup){
            console.log(`Cannot found group`);
            return;
        }
        let sound =  foundGroup[0].sfx.filter(bgm => bgm.name == soundKey);
        if(!sound){
            console.log(`Cannot found sound ${soundKey}`);
            return;
        }

        let soundPath = sound[0].dir;
        if(this._soundClips.has(soundPath)){
            const sfxNode = new Node(`sfx ${soundKey}`);
            sfxNode.setParent(this._component.node);
            let sfxAudioSource = sfxNode.addComponent(AudioSource);
            sfxAudioSource.clip = this._soundClips.get(soundPath);
            sfxAudioSource.loop = false;
            sfxAudioSource.play();
            sfxNode.addComponent(SfxDestroyWhenEnded);
            this._sfxAudioSource.push(sfxAudioSource);
        }
    }

    public stopSfx(){

    }

    public setBgmVolume(volume : number){
        if(this._bgmAudioSource){
            this._bgmAudioSource.volume = volume;
        }
    }

    public setSfxVolume(volume : number){
        for(let sfxindex = 0 ; sfxindex <  this._sfxAudioSource.length; sfxindex++){
            this._sfxAudioSource[sfxindex].volume = volume;
        }
    }
}


