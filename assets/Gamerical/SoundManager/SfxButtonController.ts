import { _decorator, CCString, Component, Input, Node } from 'cc';
import { GameDirector } from '../Utility/GameDirector';
const { ccclass, property } = _decorator;

@ccclass('SfxButtonController')
export class SfxButtonController extends Component {
    @property({
        visible : true
    })
    _sfxName : string = "";

    start() {
        if(this._sfxName){
            this.node.on(Input.EventType.TOUCH_START,() => {
                GameDirector.SoundManager().playSfx(this._sfxName);
            },this)
        }
    }

    protected onDestroy(): void {
        if(this._sfxName){
            this.node.off(Input.EventType.TOUCH_START,() => {
                GameDirector.SoundManager().playSfx(this._sfxName);
            },this)
        }
    }

   
}


