export enum UploadImageDataType{
    FILE_DATA,
    FILE_PATH
} 

export class UploadFileInfo{
    public _type : UploadImageDataType;
    public _data :  string;

    constructor(type : UploadImageDataType, data : string ){
        this._data = data;
        this._type = type;
    }
}