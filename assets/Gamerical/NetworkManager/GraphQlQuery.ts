import { UploadFileInfo } from "./UploadFileInfo";


export class GraphQlQuery{
    public _query : string;
    public _variable : string
    public _files : { [key: string]: UploadFileInfo } = {}

    public constructor(query : string, variable : string, files : { [key: string]: UploadFileInfo } ){
        this._query = query;
        this._files = files;
        this._variable = variable;
    }
    
}