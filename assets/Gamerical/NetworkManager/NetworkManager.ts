import { _decorator, Component, Node, url } from "cc";
import { MonoSingleton } from "../Utility/MonoSingleton";
import { NetworkManagerComponent } from "./NetworkManagerComponent";
import { INetworkData } from "./INetworkData";
import { GraphQlQuery } from "./GraphQlQuery";
import { UploadFileInfo } from "./UploadFileInfo";
const { ccclass, property } = _decorator;




@ccclass("NetworkManager")
export class NetworkManager extends MonoSingleton<
  NetworkManagerComponent,
  NetWorkManagerConfig
> {


  public sendGrapQlAsync<T extends INetworkData>(
    baseUr: string,
    headers: { [key: string]: string },
    requestBody: GraphQlQuery,
  ) {
    return new Promise((resolve, reject) => {
        const jsonString = JSON.stringify(requestBody);
        //just use empty file first
        let files :  { [key: string]: string };
        this.sendHttpRequest(baseUr,"graphql","POST",headers,jsonString,files)
        .then( result => {
            console.log(result);
            var obj : T = JSON.parse(result as string);
            resolve(obj);
        })
        .catch( err => {
            reject(err);
        });
    });    
  }

  public sendHttpRequest(
    baseUrl: string,
    path: string,
    method: string,
    headers: { [key: string]: string },
    requestBody: string,
    files: { [key: string]: string }
  ) {
    return new Promise(async (resolve, reject) => {
      console.log(`Request body ${requestBody}`);
      let requestSent = false;
      let retrires = 0;

      while (!requestSent) {
        try {
          const url = new URL(path, baseUrl);

          const options: RequestInit = {
            method: method,
            headers: headers,
          };

          if (requestBody.length > 0) {
            options.body = JSON.stringify(requestBody);
          }

          if (Object.keys(files).length > 0) {
            const formData = new FormData();
            for (const key in files) {
              if (Object.prototype.hasOwnProperty.call(files, key)) {
                formData.append(key, files[key]);
              }
            }
            options.body = formData;
          }

          const response = await fetch(url.toString(), options);
          console.log(`sendHttpRequest response ${response}`);
          if (!response.ok) {
            retrires++;
            if (retrires > this._config.max_retry_time) {
              throw new Error(`HTTP error! Status: ${response.status}`);
            }
            await new Promise((resolve) => setTimeout(resolve, this._config.retry_interval));
            continue;
          }
          const responseData = await response.json();
          console.log(`Receive ${responseData}`);
          resolve(responseData);
        } catch (error) {
          reject(error);
        }
      }
    });
  }
}
