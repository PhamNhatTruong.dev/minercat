interface NetWorkManagerConfig{
    max_retry_time : number;
    retry_interval : number;
    time_out : number;
}